package osgiBundle;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


public class OsgiBundleActivator implements BundleActivator { 

	@Override
	public void start(BundleContext bundleContext) throws Exception {
		
		System.out.println(".............Bundle is Started...................");
	}
		
	@Override
	public void stop(BundleContext bundleContext) throws Exception {
		
		System.out.println(".............Bundle is Stopping...................");
		
	}

}