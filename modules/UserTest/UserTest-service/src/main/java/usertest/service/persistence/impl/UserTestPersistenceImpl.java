/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package usertest.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import usertest.exception.NoSuchUserTestException;

import usertest.model.UserTest;

import usertest.model.impl.UserTestImpl;
import usertest.model.impl.UserTestModelImpl;

import usertest.service.persistence.UserTestPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the user test service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserTestPersistence
 * @see usertest.service.persistence.UserTestUtil
 * @generated
 */
@ProviderType
public class UserTestPersistenceImpl extends BasePersistenceImpl<UserTest>
	implements UserTestPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UserTestUtil} to access the user test persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UserTestImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UserTestModelImpl.ENTITY_CACHE_ENABLED,
			UserTestModelImpl.FINDER_CACHE_ENABLED, UserTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UserTestModelImpl.ENTITY_CACHE_ENABLED,
			UserTestModelImpl.FINDER_CACHE_ENABLED, UserTestImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UserTestModelImpl.ENTITY_CACHE_ENABLED,
			UserTestModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public UserTestPersistenceImpl() {
		setModelClass(UserTest.class);
	}

	/**
	 * Caches the user test in the entity cache if it is enabled.
	 *
	 * @param userTest the user test
	 */
	@Override
	public void cacheResult(UserTest userTest) {
		entityCache.putResult(UserTestModelImpl.ENTITY_CACHE_ENABLED,
			UserTestImpl.class, userTest.getPrimaryKey(), userTest);

		userTest.resetOriginalValues();
	}

	/**
	 * Caches the user tests in the entity cache if it is enabled.
	 *
	 * @param userTests the user tests
	 */
	@Override
	public void cacheResult(List<UserTest> userTests) {
		for (UserTest userTest : userTests) {
			if (entityCache.getResult(UserTestModelImpl.ENTITY_CACHE_ENABLED,
						UserTestImpl.class, userTest.getPrimaryKey()) == null) {
				cacheResult(userTest);
			}
			else {
				userTest.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all user tests.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(UserTestImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the user test.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UserTest userTest) {
		entityCache.removeResult(UserTestModelImpl.ENTITY_CACHE_ENABLED,
			UserTestImpl.class, userTest.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<UserTest> userTests) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UserTest userTest : userTests) {
			entityCache.removeResult(UserTestModelImpl.ENTITY_CACHE_ENABLED,
				UserTestImpl.class, userTest.getPrimaryKey());
		}
	}

	/**
	 * Creates a new user test with the primary key. Does not add the user test to the database.
	 *
	 * @param UserID the primary key for the new user test
	 * @return the new user test
	 */
	@Override
	public UserTest create(int UserID) {
		UserTest userTest = new UserTestImpl();

		userTest.setNew(true);
		userTest.setPrimaryKey(UserID);

		return userTest;
	}

	/**
	 * Removes the user test with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param UserID the primary key of the user test
	 * @return the user test that was removed
	 * @throws NoSuchUserTestException if a user test with the primary key could not be found
	 */
	@Override
	public UserTest remove(int UserID) throws NoSuchUserTestException {
		return remove((Serializable)UserID);
	}

	/**
	 * Removes the user test with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the user test
	 * @return the user test that was removed
	 * @throws NoSuchUserTestException if a user test with the primary key could not be found
	 */
	@Override
	public UserTest remove(Serializable primaryKey)
		throws NoSuchUserTestException {
		Session session = null;

		try {
			session = openSession();

			UserTest userTest = (UserTest)session.get(UserTestImpl.class,
					primaryKey);

			if (userTest == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUserTestException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(userTest);
		}
		catch (NoSuchUserTestException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UserTest removeImpl(UserTest userTest) {
		userTest = toUnwrappedModel(userTest);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(userTest)) {
				userTest = (UserTest)session.get(UserTestImpl.class,
						userTest.getPrimaryKeyObj());
			}

			if (userTest != null) {
				session.delete(userTest);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (userTest != null) {
			clearCache(userTest);
		}

		return userTest;
	}

	@Override
	public UserTest updateImpl(UserTest userTest) {
		userTest = toUnwrappedModel(userTest);

		boolean isNew = userTest.isNew();

		Session session = null;

		try {
			session = openSession();

			if (userTest.isNew()) {
				session.save(userTest);

				userTest.setNew(false);
			}
			else {
				userTest = (UserTest)session.merge(userTest);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		entityCache.putResult(UserTestModelImpl.ENTITY_CACHE_ENABLED,
			UserTestImpl.class, userTest.getPrimaryKey(), userTest, false);

		userTest.resetOriginalValues();

		return userTest;
	}

	protected UserTest toUnwrappedModel(UserTest userTest) {
		if (userTest instanceof UserTestImpl) {
			return userTest;
		}

		UserTestImpl userTestImpl = new UserTestImpl();

		userTestImpl.setNew(userTest.isNew());
		userTestImpl.setPrimaryKey(userTest.getPrimaryKey());

		userTestImpl.setUserID(userTest.getUserID());
		userTestImpl.setUserName(userTest.getUserName());

		return userTestImpl;
	}

	/**
	 * Returns the user test with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the user test
	 * @return the user test
	 * @throws NoSuchUserTestException if a user test with the primary key could not be found
	 */
	@Override
	public UserTest findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUserTestException {
		UserTest userTest = fetchByPrimaryKey(primaryKey);

		if (userTest == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUserTestException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return userTest;
	}

	/**
	 * Returns the user test with the primary key or throws a {@link NoSuchUserTestException} if it could not be found.
	 *
	 * @param UserID the primary key of the user test
	 * @return the user test
	 * @throws NoSuchUserTestException if a user test with the primary key could not be found
	 */
	@Override
	public UserTest findByPrimaryKey(int UserID) throws NoSuchUserTestException {
		return findByPrimaryKey((Serializable)UserID);
	}

	/**
	 * Returns the user test with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the user test
	 * @return the user test, or <code>null</code> if a user test with the primary key could not be found
	 */
	@Override
	public UserTest fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(UserTestModelImpl.ENTITY_CACHE_ENABLED,
				UserTestImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		UserTest userTest = (UserTest)serializable;

		if (userTest == null) {
			Session session = null;

			try {
				session = openSession();

				userTest = (UserTest)session.get(UserTestImpl.class, primaryKey);

				if (userTest != null) {
					cacheResult(userTest);
				}
				else {
					entityCache.putResult(UserTestModelImpl.ENTITY_CACHE_ENABLED,
						UserTestImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(UserTestModelImpl.ENTITY_CACHE_ENABLED,
					UserTestImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return userTest;
	}

	/**
	 * Returns the user test with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param UserID the primary key of the user test
	 * @return the user test, or <code>null</code> if a user test with the primary key could not be found
	 */
	@Override
	public UserTest fetchByPrimaryKey(int UserID) {
		return fetchByPrimaryKey((Serializable)UserID);
	}

	@Override
	public Map<Serializable, UserTest> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, UserTest> map = new HashMap<Serializable, UserTest>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			UserTest userTest = fetchByPrimaryKey(primaryKey);

			if (userTest != null) {
				map.put(primaryKey, userTest);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(UserTestModelImpl.ENTITY_CACHE_ENABLED,
					UserTestImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (UserTest)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_USERTEST_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (UserTest userTest : (List<UserTest>)q.list()) {
				map.put(userTest.getPrimaryKeyObj(), userTest);

				cacheResult(userTest);

				uncachedPrimaryKeys.remove(userTest.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(UserTestModelImpl.ENTITY_CACHE_ENABLED,
					UserTestImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the user tests.
	 *
	 * @return the user tests
	 */
	@Override
	public List<UserTest> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user tests.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user tests
	 * @param end the upper bound of the range of user tests (not inclusive)
	 * @return the range of user tests
	 */
	@Override
	public List<UserTest> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the user tests.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user tests
	 * @param end the upper bound of the range of user tests (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user tests
	 */
	@Override
	public List<UserTest> findAll(int start, int end,
		OrderByComparator<UserTest> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user tests.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user tests
	 * @param end the upper bound of the range of user tests (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of user tests
	 */
	@Override
	public List<UserTest> findAll(int start, int end,
		OrderByComparator<UserTest> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UserTest> list = null;

		if (retrieveFromCache) {
			list = (List<UserTest>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_USERTEST);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USERTEST;

				if (pagination) {
					sql = sql.concat(UserTestModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UserTest>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserTest>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the user tests from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (UserTest userTest : findAll()) {
			remove(userTest);
		}
	}

	/**
	 * Returns the number of user tests.
	 *
	 * @return the number of user tests
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USERTEST);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return UserTestModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the user test persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(UserTestImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_USERTEST = "SELECT userTest FROM UserTest userTest";
	private static final String _SQL_SELECT_USERTEST_WHERE_PKS_IN = "SELECT userTest FROM UserTest userTest WHERE UserID IN (";
	private static final String _SQL_COUNT_USERTEST = "SELECT COUNT(userTest) FROM UserTest userTest";
	private static final String _ORDER_BY_ENTITY_ALIAS = "userTest.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UserTest exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(UserTestPersistenceImpl.class);
}