/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package usertest.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import usertest.model.UserTest;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing UserTest in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see UserTest
 * @generated
 */
@ProviderType
public class UserTestCacheModel implements CacheModel<UserTest>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserTestCacheModel)) {
			return false;
		}

		UserTestCacheModel userTestCacheModel = (UserTestCacheModel)obj;

		if (UserID == userTestCacheModel.UserID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, UserID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{UserID=");
		sb.append(UserID);
		sb.append(", UserName=");
		sb.append(UserName);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UserTest toEntityModel() {
		UserTestImpl userTestImpl = new UserTestImpl();

		userTestImpl.setUserID(UserID);

		if (UserName == null) {
			userTestImpl.setUserName(StringPool.BLANK);
		}
		else {
			userTestImpl.setUserName(UserName);
		}

		userTestImpl.resetOriginalValues();

		return userTestImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		UserID = objectInput.readInt();
		UserName = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(UserID);

		if (UserName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(UserName);
		}
	}

	public int UserID;
	public String UserName;
}