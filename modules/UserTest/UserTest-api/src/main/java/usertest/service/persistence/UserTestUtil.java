/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package usertest.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import usertest.model.UserTest;

import java.util.List;

/**
 * The persistence utility for the user test service. This utility wraps {@link usertest.service.persistence.impl.UserTestPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserTestPersistence
 * @see usertest.service.persistence.impl.UserTestPersistenceImpl
 * @generated
 */
@ProviderType
public class UserTestUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(UserTest userTest) {
		getPersistence().clearCache(userTest);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<UserTest> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<UserTest> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<UserTest> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<UserTest> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static UserTest update(UserTest userTest) {
		return getPersistence().update(userTest);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static UserTest update(UserTest userTest,
		ServiceContext serviceContext) {
		return getPersistence().update(userTest, serviceContext);
	}

	/**
	* Caches the user test in the entity cache if it is enabled.
	*
	* @param userTest the user test
	*/
	public static void cacheResult(UserTest userTest) {
		getPersistence().cacheResult(userTest);
	}

	/**
	* Caches the user tests in the entity cache if it is enabled.
	*
	* @param userTests the user tests
	*/
	public static void cacheResult(List<UserTest> userTests) {
		getPersistence().cacheResult(userTests);
	}

	/**
	* Creates a new user test with the primary key. Does not add the user test to the database.
	*
	* @param UserID the primary key for the new user test
	* @return the new user test
	*/
	public static UserTest create(int UserID) {
		return getPersistence().create(UserID);
	}

	/**
	* Removes the user test with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param UserID the primary key of the user test
	* @return the user test that was removed
	* @throws NoSuchUserTestException if a user test with the primary key could not be found
	*/
	public static UserTest remove(int UserID)
		throws usertest.exception.NoSuchUserTestException {
		return getPersistence().remove(UserID);
	}

	public static UserTest updateImpl(UserTest userTest) {
		return getPersistence().updateImpl(userTest);
	}

	/**
	* Returns the user test with the primary key or throws a {@link NoSuchUserTestException} if it could not be found.
	*
	* @param UserID the primary key of the user test
	* @return the user test
	* @throws NoSuchUserTestException if a user test with the primary key could not be found
	*/
	public static UserTest findByPrimaryKey(int UserID)
		throws usertest.exception.NoSuchUserTestException {
		return getPersistence().findByPrimaryKey(UserID);
	}

	/**
	* Returns the user test with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param UserID the primary key of the user test
	* @return the user test, or <code>null</code> if a user test with the primary key could not be found
	*/
	public static UserTest fetchByPrimaryKey(int UserID) {
		return getPersistence().fetchByPrimaryKey(UserID);
	}

	public static java.util.Map<java.io.Serializable, UserTest> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the user tests.
	*
	* @return the user tests
	*/
	public static List<UserTest> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the user tests.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user tests
	* @param end the upper bound of the range of user tests (not inclusive)
	* @return the range of user tests
	*/
	public static List<UserTest> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the user tests.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user tests
	* @param end the upper bound of the range of user tests (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user tests
	*/
	public static List<UserTest> findAll(int start, int end,
		OrderByComparator<UserTest> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the user tests.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserTestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user tests
	* @param end the upper bound of the range of user tests (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of user tests
	*/
	public static List<UserTest> findAll(int start, int end,
		OrderByComparator<UserTest> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the user tests from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of user tests.
	*
	* @return the number of user tests
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static UserTestPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<UserTestPersistence, UserTestPersistence> _serviceTracker =
		ServiceTrackerFactory.open(UserTestPersistence.class);
}