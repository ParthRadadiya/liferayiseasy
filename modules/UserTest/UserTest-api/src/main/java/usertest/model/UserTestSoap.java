/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package usertest.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link usertest.service.http.UserTestServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see usertest.service.http.UserTestServiceSoap
 * @generated
 */
@ProviderType
public class UserTestSoap implements Serializable {
	public static UserTestSoap toSoapModel(UserTest model) {
		UserTestSoap soapModel = new UserTestSoap();

		soapModel.setUserID(model.getUserID());
		soapModel.setUserName(model.getUserName());

		return soapModel;
	}

	public static UserTestSoap[] toSoapModels(UserTest[] models) {
		UserTestSoap[] soapModels = new UserTestSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static UserTestSoap[][] toSoapModels(UserTest[][] models) {
		UserTestSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new UserTestSoap[models.length][models[0].length];
		}
		else {
			soapModels = new UserTestSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static UserTestSoap[] toSoapModels(List<UserTest> models) {
		List<UserTestSoap> soapModels = new ArrayList<UserTestSoap>(models.size());

		for (UserTest model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new UserTestSoap[soapModels.size()]);
	}

	public UserTestSoap() {
	}

	public int getPrimaryKey() {
		return _UserID;
	}

	public void setPrimaryKey(int pk) {
		setUserID(pk);
	}

	public int getUserID() {
		return _UserID;
	}

	public void setUserID(int UserID) {
		_UserID = UserID;
	}

	public String getUserName() {
		return _UserName;
	}

	public void setUserName(String UserName) {
		_UserName = UserName;
	}

	private int _UserID;
	private String _UserName;
}