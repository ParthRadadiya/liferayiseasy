/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package usertest.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the UserTest service. Represents a row in the &quot;UserTest_UserTest&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see UserTestModel
 * @see usertest.model.impl.UserTestImpl
 * @see usertest.model.impl.UserTestModelImpl
 * @generated
 */
@ImplementationClassName("usertest.model.impl.UserTestImpl")
@ProviderType
public interface UserTest extends UserTestModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link usertest.model.impl.UserTestImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<UserTest, Integer> USER_I_D_ACCESSOR = new Accessor<UserTest, Integer>() {
			@Override
			public Integer get(UserTest userTest) {
				return userTest.getUserID();
			}

			@Override
			public Class<Integer> getAttributeClass() {
				return Integer.class;
			}

			@Override
			public Class<UserTest> getTypeClass() {
				return UserTest.class;
			}
		};
}