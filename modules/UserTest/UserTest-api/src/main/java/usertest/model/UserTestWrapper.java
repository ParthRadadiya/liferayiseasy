/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package usertest.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link UserTest}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserTest
 * @generated
 */
@ProviderType
public class UserTestWrapper implements UserTest, ModelWrapper<UserTest> {
	public UserTestWrapper(UserTest userTest) {
		_userTest = userTest;
	}

	@Override
	public Class<?> getModelClass() {
		return UserTest.class;
	}

	@Override
	public String getModelClassName() {
		return UserTest.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("UserID", getUserID());
		attributes.put("UserName", getUserName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer UserID = (Integer)attributes.get("UserID");

		if (UserID != null) {
			setUserID(UserID);
		}

		String UserName = (String)attributes.get("UserName");

		if (UserName != null) {
			setUserName(UserName);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _userTest.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _userTest.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _userTest.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _userTest.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<usertest.model.UserTest> toCacheModel() {
		return _userTest.toCacheModel();
	}

	@Override
	public int compareTo(usertest.model.UserTest userTest) {
		return _userTest.compareTo(userTest);
	}

	/**
	* Returns the primary key of this user test.
	*
	* @return the primary key of this user test
	*/
	@Override
	public int getPrimaryKey() {
		return _userTest.getPrimaryKey();
	}

	/**
	* Returns the user i d of this user test.
	*
	* @return the user i d of this user test
	*/
	@Override
	public int getUserID() {
		return _userTest.getUserID();
	}

	@Override
	public int hashCode() {
		return _userTest.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _userTest.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new UserTestWrapper((UserTest)_userTest.clone());
	}

	/**
	* Returns the user name of this user test.
	*
	* @return the user name of this user test
	*/
	@Override
	public java.lang.String getUserName() {
		return _userTest.getUserName();
	}

	@Override
	public java.lang.String toString() {
		return _userTest.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _userTest.toXmlString();
	}

	@Override
	public usertest.model.UserTest toEscapedModel() {
		return new UserTestWrapper(_userTest.toEscapedModel());
	}

	@Override
	public usertest.model.UserTest toUnescapedModel() {
		return new UserTestWrapper(_userTest.toUnescapedModel());
	}

	@Override
	public void persist() {
		_userTest.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_userTest.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_userTest.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_userTest.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_userTest.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public void setNew(boolean n) {
		_userTest.setNew(n);
	}

	/**
	* Sets the primary key of this user test.
	*
	* @param primaryKey the primary key of this user test
	*/
	@Override
	public void setPrimaryKey(int primaryKey) {
		_userTest.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_userTest.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the user i d of this user test.
	*
	* @param UserID the user i d of this user test
	*/
	@Override
	public void setUserID(int UserID) {
		_userTest.setUserID(UserID);
	}

	/**
	* Sets the user name of this user test.
	*
	* @param UserName the user name of this user test
	*/
	@Override
	public void setUserName(java.lang.String UserName) {
		_userTest.setUserName(UserName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserTestWrapper)) {
			return false;
		}

		UserTestWrapper userTestWrapper = (UserTestWrapper)obj;

		if (Objects.equals(_userTest, userTestWrapper._userTest)) {
			return true;
		}

		return false;
	}

	@Override
	public UserTest getWrappedModel() {
		return _userTest;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _userTest.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _userTest.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_userTest.resetOriginalValues();
	}

	private final UserTest _userTest;
}