package com.dynamicQuery.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.dynamicQuery.constants.DynamicQuerySamplePortletKeys;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactory;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import usertest.model.UserTest;
import usertest.service.UserTestLocalService;

/**
 * @author parth.radadiya
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + DynamicQuerySamplePortletKeys.DynamicQuerySample,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)

public class DynamicQuerySamplePortlet extends MVCPortlet {

	private static final Log _Log = LogFactoryUtil.getLog(DynamicQuerySamplePortlet.class);

	@Reference
	DynamicQueryFactory dynamicQueryFactory;
	@Reference
	RestrictionsFactory restrictionsFactory;
	@Reference
	UserTestLocalService userTestLocalService;

	public void dynamicFind(ActionRequest request, ActionResponse response) throws PortletException, IOException {

		_Log.info("dynamicFind() Method called");

		DynamicQuery dynamicQuery = dynamicQueryFactory.forClass(UserTest.class, "User",
				PortletClassLoaderUtil.getClassLoader());
		dynamicQuery.add(restrictionsFactory.eq("User.UserName", "Parth"));
		List<UserTest> userTestList = (List<UserTest>) userTestLocalService.dynamicQuery();

		for (UserTest userTest : userTestList) {

			_Log.info("Name : " + userTest.getUserName());

		}

	}
}